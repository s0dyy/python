# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 ]

SUMMARY="A self-contained Python package of low-level cryptographic primitives"
DESCRIPTION="PyCryptodome is a fork of the now unmaintained PyCrypto. It
brings several enhancements with respect to the last official version of
PyCrypto (2.6.1).
It is not a wrapper to a separate C library like *OpenSSL*. To the largest
possible extent, algorithms are implemented in pure Python. Only the pieces
that are extremely critical to performance (e.g. block ciphers) are
implemented as C extensions."

HOMEPAGE="https://www.pycryptodome.org ${HOMEPAGE}"

LICENCES="
    Apache-2.0
    BSD-2
    public-domain
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !dev-python/pycrypto [[
            description = [ pycrypto and pycryptodome install colliding files ]
            resolution = uninstall-blocked-after
        ]]
    run:
        dev-libs/gmp:*
"

#TODO: unbundle libtomcrypt -> https://github.com/libtom/libtomcrypt

BUGS_TO="heirecka@exherbo.org"

